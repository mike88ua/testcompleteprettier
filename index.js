const express = require("express");
const path = require("path");
const PORT = process.env.PORT || 5999;
const prettierApi = require("./controllers/prittier-api");
const bodyParser = require("body-parser");
let options;
try {
  options = require('./prettier-options.json');
} catch (e) {
  console.log(e, 'prettier-options.json file doesn\'t exist')
}

express()
  .post(
    "/api/prettier",
    bodyParser.urlencoded({limit: 1024102420, type:'application/x-www-form-urlencoded'}),
    prettierApi.prettierHandler(options)
  )
  .listen(PORT, () => console.log(`Listening on ${PORT}`));
