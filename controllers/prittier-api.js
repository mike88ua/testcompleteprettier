const prettier = require("prettier");
module.exports = {
  prettierHandler(options) {
    return (req, res) => {
      try {
        console.log('body.code: ', req.body.code);
        const data = req.body.code;
        console.log('Decoded Data: ', data);
        const prettierCode = prettier.format(data, options);
        console.log('prettierCode', prettierCode);
        console.log('Done.');
        res.status(200).send(prettierCode);
      } catch (e) {
        console.log('Error', e);
        res.status(400).send(null)
      }
    }
  }
};
