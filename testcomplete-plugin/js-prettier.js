function DesignTimeAction() {
  if (!CodeEditor.IsEditorActive) return;
  var newCode = prettify(CodeEditor.Text);
  if (null == newCode) return;
  CodeEditor.Text = newCode;
}

function prettify(code) {
  var URL = "http://localhost:5999/api/prettier";

  var httpObj = Sys.OleObject("MSXML2.XMLHTTP");

  httpObj.open("POST", URL, false);
  httpObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  httpObj.send("code=" + encodeURIComponent(code));

  var responseText = httpObj.responseText;
  return httpObj.status === 200 && responseText ? responseText : null;
}